package dashboard.beans;

import lombok.extern.slf4j.Slf4j;

import javax.ejb.Singleton;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;

/**
 * bean which pushing messages to websocket
 * @author Ivan Rud
 */
@Singleton
@Slf4j
public class PushBean {
    @Inject
    @Push(channel = "websocket")
    PushContext pushContext;

    public void sendMessage(String message) {
        log.info("pushing to context");
        pushContext.send(message);
    }
}
