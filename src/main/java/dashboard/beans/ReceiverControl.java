package dashboard.beans;

import com.rabbitmq.client.*;
import dashboard.analysis.ChartCreator;
import dashboard.json.JSONReader;
import dashboard.restmodels.*;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.PieChartModel;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * main worker class
 * @author Ivan Rud
 */
@Named
@Startup
@Stateless
@Dependent
@Slf4j
public class ReceiverControl {

    @Inject
    PushBean pushBean;

    PieChartModel ordersChart;
    PieChartModel driversChart;
    PieChartModel trucksConditionChart;
    PieChartModel trucksStatusChart;

    HorizontalBarChartModel driversHoursChart;

    Channel channel;
    private final static String QUEUE_NAME = "logiweb";

    private static List<OrderRest> orders;
    private static List<DriverRest> drivers;
    private static List<TruckRest> trucks;

    /**
     * listening for messages from mq
     * @throws IOException problems with connection
     */
    public void listen() throws IOException {
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + message + "'");

            log.info("listening for a message");

            if(message.equals("updated")) {
                reads();
            }

            if(message.equals("driversUpd")) {
                drivers = JSONReader.readDrivers("https://ruddi-logiweb.herokuapp.com/drivers/list");
                driversHoursChart = ChartCreator.driversHoursChartModel(drivers);
                driversChart = ChartCreator.driversPieChartFill(drivers);
            }

            if(message.equals("trucksUpd")) {
                trucks = JSONReader.readTrucks("https://ruddi-logiweb.herokuapp.com/trucks/list");
                trucksConditionChart = ChartCreator.trucksConditionPieChartFill(trucks);
                trucksStatusChart = ChartCreator.trucksStatusPieChartFill(trucks);
            }

            pushBean.sendMessage("upd");
        };

        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
    }

    /**
     * rest reading
     * @throws IOException problems
     */
    private void reads() throws IOException {
        log.info("requests to rest");

        orders = JSONReader.readOrders("https://ruddi-logiweb.herokuapp.com/orders");
        drivers = JSONReader.readDrivers("https://ruddi-logiweb.herokuapp.com/drivers/list");
        trucks = JSONReader.readTrucks("https://ruddi-logiweb.herokuapp.com/trucks/list");
        ordersChart = ChartCreator.ordersPieChartFill(orders);
        driversHoursChart = ChartCreator.driversHoursChartModel(drivers);
        driversChart = ChartCreator.driversPieChartFill(drivers);
        trucksConditionChart = ChartCreator.trucksConditionPieChartFill(trucks);
        trucksStatusChart = ChartCreator.trucksStatusPieChartFill(trucks);
    }

    @PostConstruct
    public void init() throws Exception {
        log.info("initialization");

        ordersChart = new PieChartModel();
        driversChart = new PieChartModel();
        trucksConditionChart = new PieChartModel();
        trucksStatusChart = new PieChartModel();
        driversHoursChart = new HorizontalBarChartModel();

        ConnectionFactory factory = new ConnectionFactory();

        String uri = "amqp://sxtswmgm:dh1N5aBEUnam53urt2VMrF9HSi7IDWAf@stingray.rmq.cloudamqp.com/sxtswmgm";

        try {
            factory.setUri(uri);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare("logiweb", false, false, false, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        reads();
        listen();
    }

    public List<OrderRest> getOrders() {
        return orders;
    }

    public PieChartModel getOrdersChart() {
        return ordersChart;
    }

    public HorizontalBarChartModel getDriversHoursChart() {
        return driversHoursChart;
    }

    public PieChartModel getDriversChart() {
        return driversChart;
    }

    public PieChartModel getTrucksConditionChart () {
        return trucksConditionChart;
    }

    public PieChartModel getTrucksStatusChart() {
        return trucksStatusChart;
    }
}
