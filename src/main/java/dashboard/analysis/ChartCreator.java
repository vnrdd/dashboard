package dashboard.analysis;

import dashboard.restmodels.DriverRest;
import dashboard.restmodels.OrderRest;
import dashboard.restmodels.TruckRest;
import org.primefaces.model.chart.*;

import java.util.List;

/**
 * chart creation class
 * @author Ivan Rud
 */
public class ChartCreator {
    public static PieChartModel ordersPieChartFill(List<OrderRest> orders) {
        PieChartModel ordersChart = new PieChartModel();
        int preparing = 0;
        int readyToTakeOff = 0;
        int inProgress = 0;
        int closed = 0;

        for (OrderRest order : orders) {
            if (order.getStatus().equals("Preparing")) ++preparing;
            if (order.getStatus().equals("Ready to take off")) ++readyToTakeOff;
            if (order.getStatus().equals("In progress")) ++inProgress;
            if (order.getStatus().equals("Completed")) ++closed;
        }

        ordersChart.set("Preparing", preparing);
        ordersChart.set("Ready to take off", readyToTakeOff);
        ordersChart.set("In progress", inProgress);
        ordersChart.set("Closed", closed);

        ordersChart.setTitle("Orders statuses");
        ordersChart.setLegendPosition("e");
        ordersChart.setShowDatatip(true);
        ordersChart.setShowDataLabels(true);
        ordersChart.setDataFormat("value");
        ordersChart.setDataLabelFormatString("%dK");
        ordersChart.setSeriesColors("aaf,afa,faa,ffa");

        return ordersChart;
    }

    public static PieChartModel driversPieChartFill(List<DriverRest> drivers) {
        PieChartModel driversChart = new PieChartModel();
        int resting = 0;
        int onShift = 0;
        int driving = 0;

        for (DriverRest driver : drivers) {
            if (driver.getStatus().equals("Resting")) ++resting;
            if (driver.getStatus().equals("On shift")) ++onShift;
            if (driver.getStatus().equals("Driving")) ++driving;
        }

        driversChart.set("Resting", resting);
        driversChart.set("On shift", onShift);
        driversChart.set("Driving", driving);

        driversChart.setTitle("Drivers statuses");
        driversChart.setLegendPosition("e");
        driversChart.setShowDatatip(true);
        driversChart.setShowDataLabels(true);
        driversChart.setDataFormat("value");
        driversChart.setDataLabelFormatString("%dK");
        driversChart.setSeriesColors("aaf,afa,faa,ffa");

        return driversChart;
    }

    public static PieChartModel trucksConditionPieChartFill(List<TruckRest> trucks) {
        PieChartModel chart = new PieChartModel();
        int ok = 0;
        int defective= 0;

        for (TruckRest truck : trucks) {
            if (truck.getCondition().equals("OK")) ++ok;
            if (truck.getCondition().equals("Defective")) ++defective;
        }

        chart.set("OK", ok);
        chart.set("Defective", defective);

        chart.setTitle("Trucks conditions");
        chart.setLegendPosition("e");
        chart.setShowDatatip(true);
        chart.setShowDataLabels(true);
        chart.setDataFormat("value");
        chart.setDataLabelFormatString("%dK");
        chart.setSeriesColors("aaf,afa,faa,ffa");

        return chart;
  }

    public static PieChartModel trucksStatusPieChartFill(List<TruckRest> trucks) {
        PieChartModel chart = new PieChartModel();
        int free = 0;
        int booked = 0;
        int busy = 0;

        for (TruckRest truck : trucks) {
            if (truck.getStatus().equals("Free")) ++free;
            if (truck.getStatus().equals("Booked")) ++booked;
            if (truck.getStatus().equals("Busy")) ++busy;
        }

        chart.set("Free", free);
        chart.set("Booked", booked);
        chart.set("Busy", busy);

        chart.setTitle("Trucks statuses");
        chart.setLegendPosition("e");
        chart.setShowDatatip(true);
        chart.setShowDataLabels(true);
        chart.setDataFormat("value");
        chart.setDataLabelFormatString("%dK");
        chart.setSeriesColors("aaf,afa,faa,ffa");

        return chart;
    }

    public static HorizontalBarChartModel driversHoursChartModel (List<DriverRest> drivers) {
        HorizontalBarChartModel barChart = new HorizontalBarChartModel();

        ChartSeries hours = new ChartSeries();

        for(DriverRest driver : drivers)
            hours.set(driver.getFirstName() + " "
                    + driver.getSecondName(), driver.getHoursWorked());

        barChart.addSeries(hours);
        barChart.setTitle("Drivers hours worked");

        Axis xAxis = barChart.getAxis(AxisType.X);
        xAxis.setLabel("Hours worked");
        xAxis.setMin(0);
        xAxis.setTickInterval("8");
        xAxis.setMax(176);

        return barChart;
    }
}
