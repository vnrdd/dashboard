package dashboard.restmodels;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class OrderRest {
    private int id;
    private String departure;
    private String destination;
    private int totalWeight;
    private String status;
}
