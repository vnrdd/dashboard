package dashboard.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import dashboard.restmodels.DriverRest;
import dashboard.restmodels.OrderRest;
import dashboard.restmodels.TruckRest;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;


public class JSONReader {
    public static List<OrderRest> readOrders(String url) throws IOException {
        return Arrays.asList((new ObjectMapper()).readValue
                (new URL(url), OrderRest[].class).clone());
    }

    public static List<DriverRest> readDrivers(String url) throws IOException {
        return Arrays.asList((new ObjectMapper()).readValue
                (new URL(url), DriverRest[].class).clone());
    }

    public static List<TruckRest> readTrucks(String url) throws IOException {
        return Arrays.asList((new ObjectMapper()).readValue
                (new URL(url), TruckRest[].class).clone());
    }
}
